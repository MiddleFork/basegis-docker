# Docker image for middlefork/basegis

Image built on top of debian:buster which provides a base set of common packages used for providing GIS capabilities and tools

## Package list (partial)

- PostgreSQL
- PostGIS
- GDAL libraries
- Proj4
- Mapserver 
- Mapnik

## Images derived from this image (partial)

- MiddleFork/mapserver
- MiddleFork/mapproxy

## How to use this image

Install the container

```
docker pull middlefork/basegis
```

Or clone the repository and build locally

```buildoutcfg
git clone https://bitbuket.org/MiddleFork/basegis-docker.git
cd basegis-docker
docker-compose build

```